#!/usr/bin/python

# Generate random wireless network scenarios
#
# (C) Copyright 2016 Emma Fitzgerald 
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import math
import random
import os
import sys
from numpy.random import seed as npseed
from numpy.random import poisson

### Parameters:

# Number of scenarios to generate
num_runs = 5
# Whether to generate a mesh network (True) or an infrastructure network (False)
mesh = False

#   Number of nodes
# For mesh network
num_nodes = 10
# For infrastructure network
num_APs = 5
stations_per_AP = 5
#   Area to distribute nodes X and Y
playground_width = 500.0  
playground_height = 500.0 
#   Distribution to use for node placement and distribution params
location_distribution = "Uniform"       # Only uniform distribution is currently implemented
location_lambda = 0.0  # Parameter for placement distribution, not currently used

#   Traffic generation params: distribution to use and distribution params
arrival_distribution = "Poisson"    # Distribution for number of packets to generate
arrival_lambda = 20                 # Average number of packets to generate
packet_length_distribution = "UniformDiscrete"     # Can be "UniformDiscrete" to choose from a set, or "Constant"
# Packet lengths 
packet_lengths = [100, 200, 300, 500, 10000]   # For "UniformDiscrete" distribution
packet_length_lambda = 500  # For constant packet length

#   Channel model, channel model params
transmission_range = 200.0  # metres

### End paramters


# Total packets generated
total_packets = 0

# Class to represent a node
class Node:
    def __init__(self, name = "", x = 0, y = 0, role = "STA"):
        self.name = name
        self.x = x
        self.y = y
        self.packets = []
        self.role = role

    def set_location(self, x, y):
        self.x = x
        self.y = y

    def queue_packets(self, new_packets):
        self.packets.extend(new_packets)

    def set_name(self, name):
        self.name = name

    def __repr__(self):
        packets_dict = self.packets_to_receivers()
        packet_string = ""
        for i in sorted(packets_dict.keys()):
            for j in xrange(len(packets_dict[i])):
                length = str(packets_dict[i][j])
                packet_string += i.name + ": " + str(length) + ", "
        return self.name + ": (" + str(self.x) + ", " + str(self.y) + "), " + \
                str(len(self.packets)) + ": " + packet_string

    def packets_to_receivers(self):
        packet_dict = {}
        for packet in self.packets:
            receiver = packet[0]
            if receiver not in packet_dict.keys():
                packet_dict[receiver] = []
            packet_dict[receiver].append(packet[1])

        return packet_dict

# Create a list of nodes
def create_nodes(num):
    nodes = []
    for i in xrange(num):
        letter = i % 26
        nodename = str(chr(letter + ord('A')))
        if i > 25:
            nodename = "A" + nodename 
        node = Node(name = nodename)
        nodes.append(node)

    return nodes

# Determine locations for a list of nodes
def place_nodes(nodes, width, height, distribution, dist_params):
    for node in nodes:
        # Generate a location within the playground
        x, y = generate_location(width, height, distribution, dist_params)

        # Assign the location to the node
        node.set_location(x, y)

# Generate a location for a node according to a given distribution
def generate_location(width, height, distribution, dist_params):
    x = 0
    y = 0
    if distribution == "Uniform":
        x = rng.uniform(0.0, width)
        y = rng.uniform(0.0, height)

    return [x, y]

# Create traffic for a list of nodes
def generate_traffic(nodes, links, arrival_dist, arr_dist_params, length_dist, 
        len_dist_params):
    for node in nodes:
        # Generate a queue of packets
        receivers = []
        for l in links:
            if l[0] == node:
                receivers.append(l[1])

        if len(receivers) == 0:
            print "No other nodes in range of node", node
            continue

        packets = generate_queue(receivers, arrival_dist, arr_dist_params, length_dist, 
                len_dist_params)

        # Assign queue to node
        node.queue_packets(packets)
        global total_packets
        total_packets += len(packets)

# Generate a queue of traffic for a node
def generate_queue(receivers, arrival_dist, arr_dist_params, length_dist, 
                        len_dist_params):
    num_packets = 0
    packets = []

    # Generate number of packets
    if arrival_dist == "Poisson":
        lambd = arr_dist_params[0]
        num_packets = poisson(lambd)

    # Generate packets
    if length_dist == "Constant":
        length = len_dist_params[0]
        for i in xrange(num_packets):
            receiver = rng.choice(receivers) 
            packets.append((receiver, length))
    elif length_dist == "UniformDiscrete":
        for i in xrange(num_packets):
            length = rng.choice(packet_lengths)
            receiver = rng.choice(receivers)
            packets.append((receiver, length))
    
    return packets

# Determine which links interfere with each other
def get_interfering_links(links):
    # 2D array of interference: interference[i][j] == True indicates link
    # i interferes with link j, and correspondingly link j is interfered by
    # link i
    interferes_with = {}
    interfered_by = {}

    for l in links:
        interferes_with[l] = [] 
        interfered_by[l] = []

    for l in links:
        for m in links:
            if l == m:
                continue
            if interferes(l, m) or l[0] == m[0] or l[1] == m[0] or m[1] == l[0]:
                interferes_with[l].append(m)
                interfered_by[m].append(l)

    return [interferes_with, interfered_by]

# Get list of links between nodes based on node locations
def get_links(nodes):
    links = []
    for i in nodes:
        for j in nodes:
            if within_range(j, i) and i != j:
                links.append((i, j))

    return links

# Returns True if node n1 is within transmission range of node n2, False
# otherwise
def within_range(n1, n2):
    return distance(n1, n2) < transmission_range

# Returns the Cartesian distance between two nodes
def distance(n1, n2):
    return math.sqrt(math.pow(n1.x - n2.x, 2) + math.pow(n1.y - n2.y, 2))

# Returns True if l1 interferes with l2, False otherwise
def interferes(l1, l2):
    # Return true if receiver of l2 is within range of sender of l1
    if(within_range(l2[1], l1[0])):
        return True
    return False

# Create a network scenario with nodes, links between them, traffic at each
# node and interference between links
# Nodes communicate with all of their neighbour homogeneously
def generate_mesh_network():
    nodes = create_nodes(num_nodes)
    place_nodes(nodes, playground_width, playground_height, 
        location_distribution, [location_lambda])
    links = get_links(nodes)
    interference = get_interfering_links(links)
    generate_traffic(nodes, links, arrival_distribution, [arrival_lambda],
        packet_length_distribution, [packet_length_lambda])

    return [nodes, links, interference]

# Generates an access point and its associated nodes
def generate_access_point(start):
    nodes = []
    num = stations_per_AP
    global total_packets
    
    AP_ID = start
    letter = AP_ID % 26
    nodename = str(chr(letter + ord('A')))
    if AP_ID > 25 and AP_ID < 52:
        nodename = "A" + nodename 
    elif AP_ID > 51:
        nodename = "B" + nodename
    AP = Node(name = nodename, role = "AP")
    nodes.append(AP)

    AP_x, AP_y = generate_location(playground_width, playground_height, "Uniform", None)
    AP.set_location(AP_x, AP_y)
    
    for i in xrange(num):
        node_ID = i + start + 1
        letter = node_ID % 26
        nodename = str(chr(letter + ord('A')))
        if node_ID > 25 and node_ID < 52:
            nodename = "A" + nodename 
        elif node_ID > 51:
            nodename = "B" + nodename

        node = Node(name = nodename, role = "STA")
        nodes.append(node)
        while True:
            x, y = generate_location(transmission_range * 2, transmission_range * 2, "Uniform", None)
            x = x + AP_x - transmission_range
            y = y + AP_y - transmission_range
            if x < 0 or y < 0 or x > playground_width or y > playground_height:
                continue
            node.set_location(x, y)
            if within_range(node, AP):
                break
        
        # Generate downlink traffic (AP to node)
        packets = generate_queue([node],arrival_distribution, [arrival_lambda],
                    packet_length_distribution, [packet_length_lambda])
        AP.queue_packets(packets)
        total_packets += len(packets)

        # Generate uplink traffic (node to AP)
        packets = generate_queue([AP],arrival_distribution, [arrival_lambda],
                    packet_length_distribution, [packet_length_lambda])
        node.queue_packets(packets)
        total_packets += len(packets)

    return nodes

# As for generate_mesh_network(), except that some nodes are access points, with other nodes stations
# belonging to access points. Access points only communicate with their associated stations, and vice versa
def generate_infra_network():
    nodes = []
    for i in xrange(num_APs):
        nodes.extend(generate_access_point(len(nodes)))

    links = get_links(nodes)
    interference = get_interfering_links(links)

    return [nodes, links, interference]

# Generates an AMPL data file for the network scenario
def write_AMPL_data(nodes, links, interference, filename):
    f = open(filename, "w")

    f.write("data;\n\n")

    f.write("set all_nodes := ")
    for n in nodes:
        f.write(n.name + " ")
    f.write(";\n\n")
    
    f.write("set packets := \n")
    for node in nodes:
        for i in xrange(len(node.packets)):
            packet = node.packets[i]
            pkt_id = node.name + "_" + packet[0].name + "_" + str(i) 
            f.write("\t" + pkt_id + "\n")
    f.write(";\n\n")
        
    f.write("param:\tsender\tdest\tlength := \n")
    for node in nodes:
        for i in xrange(len(node.packets)):
            packet = node.packets[i]
            pkt_id = node.name + "_" + packet[0].name + "_" + str(i) 
            f.write(pkt_id + "\t" + node.name + "\t" + packet[0].name + "\t" + str(packet[1]) + "\n")
    f.write(";\n\n")

    f.write("set links :=\n")
    for l in links:
        f.write("\t(" + l[0].name + ", " + l[1].name + ")\n")
    f.write(";\n\n")

    for l in links:
        f.write("set interferes_with[" + l[0].name + ", " + l[1].name + "] :=\n")
        for ll in interference[0][l]:
            f.write("\t(" + ll[0].name + ", " + ll[1].name + ")\n")
        f.write(";\n")

    f.write("\n")

    for l in links:
        f.write("set interfered_by[" + l[0].name + ", " + l[1].name + "] :=\n")
        for ll in interference[1][l]:
            f.write("\t(" + ll[0].name + ", " + ll[1].name + ")\n")
        f.write(";\n")

    f.close()

# Outputs the network scenario in a human and machine readable text format
def output_network(nodes, links, interference, seed, filename):
    f = open(filename, "w")
    
    f.write("Seed:\n")
    f.write(str(seed))
    f.write("\n")

    f.write("\nNodes:\n")
    for n in nodes:
        f.write(str(n) + "\n")

    f.write("\nLinks:\n")
    for l in links:
        f.write(l[0].name + "\t" + l[1].name + "\n")

    f.write("\nInterfered by:\n")
    for l in links:
        f.write("\tLink\t" + l[0].name + "\t" + l[1].name + "\n")
        for ll in interference[1][l]:
            f.write("\t\t" + ll[0].name + "\t" + ll[1].name + "\n")

    f.close()

if __name__ == "__main__":
    if len(sys.argv) == 2: 
        base_seed = int(sys.argv[1])
    else:
        base_seed = random.randint(0, sys.maxint)

    global rng
    seed_rng = random.Random(base_seed)

    try:
        os.stat("scenarios")
    except:
        os.mkdir("scenarios") 

    for i in xrange(num_runs):
        while True:
            print "Generating scenario..."
            sys.stdout.flush()

            seed = seed_rng.randint(0, sys.maxint)
            npseed(seed)
            rng = random.Random(seed)

            total_packets = 0

            if mesh:
                print "Generating mesh network..."
                sys.stdout.flush()
                nodes, links, interference = generate_mesh_network()
            else:
                print "Generating infrastructure network..."
                sys.stdout.flush()
                nodes, links, interference = generate_infra_network()
            num_nodes = len(nodes)

            # Re-generate scenarios where there were no nodes with neighbours, or no packets generated
            if total_packets == 0:
                continue

            output_network(nodes, links, interference, seed, "scenarios/scenario_" + str(i) + ".txt")
            write_AMPL_data(nodes, links, interference, "scenarios/scenario_" + str(i) + "_AMPL.dat")

            print "Created scenario"
            sys.stdout.flush()
            
            break
