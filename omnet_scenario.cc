/** Example code for integrating the network generator with OMNeT++.
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "omnet_scenario.h"

Define_Module(CoopScenario);

Scenario::Scenario()
{
    links = LinkMap();
    nodes = NodeMap();
}

Scenario::~Scenario()
{
}

void Scenario::initialize(int stage)
{
    if(stage == 0)
    {
	// Read scenario from file
	readScenario();
    }
}

void Scenario::readScenario()
{
    FILE *file = fopen("scenario", "r");
    char line[2048];

    while(fgets(line, 2048, file))
    {
	if(strncmp(line, "Nodes:", strlen("Nodes:")) == 0)
	    parseNodes(file);
	if(strncmp(line, "Links:", strlen("Links:")) == 0)
	    parseLinks(file);
	if(strncmp(line, "Interfered by:", strlen("Interfered by:")) == 0)
	    parseInterference(file);
    }

    fclose(file);
}

void Scenario::parseNodes(FILE *file)
{
    int id = 0;

    while(1)
    {
	char line[2048];
	fgets(line, 2048, file);

	if(line[0] == '\0' || isspace(line[0]))
	    break;
	
	std::ostringstream path;
	path << "*.host[";
	path << id;
	path << "]";
	std::string pathstr = path.str();
	cModule *node = getModuleByPath(pathstr.c_str());
	cModule *app_mod = node->getSubmodule("app");	    // Change to your application module name
	CoopApp *app = check_and_cast<CoopApp *>(app_mod);

	char *name, *packets, *packets_cpy;
	float x = 0.0, y = 0.0;
	int num_packets = 0;

	sscanf(line, "%m[^:]: (%f, %f), %d: %m[^\n]", &name, &x, &y, &num_packets, &packets);
	packets_cpy = packets;

	Coord pos = Coord(x, y); 
	nodes[name] = node;

	// Parse packets
	std::queue<Packet> packet_queue = std::queue<Packet>();
	for(int i = 0; i < num_packets;) 
	{
	    char *recv_name;
	    int length = 0;
	    char *end = strstr(packets, ",");
	    *end = '\0';
	    sscanf(packets, " %m[^:]: %d ", &recv_name, &length);
	    i +=1;
	    Packet packet;
	    packet.receiver = std::string(recv_name);
	    packet.length = length;
	    packet_queue.push(packet);
	    packets = end + 1;
	    free(recv_name);
	}

	// The app module needs to implement these methods
	app->setNodeName(name);
	app->queuePackets(packet_queue);

	free(name);
	free(packets_cpy);
	id++;
    } 
}

void Scenario::parseLinks(FILE *file)
{
    char line[2048];

    while(1)
    {
	fgets(line, 2048, file);
	if(strlen(line) <= 1 || line[0] == '\n')
	    break;
	
	char *sender, *receiver;
	Link *link = new Link;
	if(link == NULL)
	    exit(-1);
	sscanf(line, "%ms %ms ", &sender, &receiver); 

	link->sender = std::string(sender);
	link->receiver = std::string(receiver);
	std::pair<std::string, std::string> key = std::pair<std::string, std::string>(link->sender, link->receiver);
	link->active = false;
	link->broadcast = false;
	link->delivery_scheduled = false;
	link->sched_order = scheduling_order[link->sender];
	links[key] = link;

	free(sender);
	free(receiver);
    }
}

void Scenario::parseInterference(FILE *file)
{
    char line[2048];
    Link *link;

    while(1)
    {
	if(fgets(line, 2048, file) == NULL)
	    break;
	if(strlen(line) <= 1 || line[0] == '\n')
	    break;

	char *s, *r;
	if(strstr(line, "Link") != NULL)
	{
	    sscanf(line, " Link %ms %ms ", &s, &r);
	    std::string sender = std::string(s);
	    std::string receiver = std::string(r);
	    std::pair<std::string, std::string> key = std::pair<std::string, std::string>(sender, receiver);
	    link = links[key];
	    free(s);
	    free(r);
	    continue;
	}

	sscanf(line, " %ms %ms ", &s, &r);
	std::string sender = std::string(s);
	std::string receiver = std::string(r);
	std::pair<std::string, std::string> key = std::pair<std::string, std::string>(sender, receiver);
	Link *interferer = links[key];
	free(s);
	free(r);

	link->interfered_by.push_back(interferer);
    }
}
