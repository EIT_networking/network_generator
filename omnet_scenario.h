/* Reads in a scenario and sets it up in OMNeT
 *
 * © Copyright 2016 Emma Fitzgerald 
 * emma.fitzgerald@eit.lth.se
 *
 * This program is distributed under the terms of the GNU Lesser General Public License.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCENARIO_H_
#define SCENARIO_H_

#include "omnetpp.h"
#include <boost/unordered_map.hpp>

class Scenario : public cSimpleModule
{
    private:
	virtual void readScenario();
	void parseNodes(FILE *file);
	void parseLinks(FILE *file);
	void parseInterference(FILE *file);

	LinkMap links;
	NodeMap nodes;

    protected:
	virtual int numInitStages() const;
	virtual void initialize(int stage);

    public:
	Scenario();
	virtual ~Scenario();
	typedef struct
	{
	    std::string receiver;
	    int length;
	} Packet;
};

#endif // SCENARIO_H_
